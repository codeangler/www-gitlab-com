---
layout: markdown_page
title: Analyst Relations at GitLab
description: Insight about AR at GitLab
---

## What are analysts saying about GitLab and how are we building off their insight and input?

## Where does GitLab Engage?

GitLab appears in several areas within the analyst arena.  Areas we engage analysts include:

  - DevOps
  - Agile Project management
  - Planning/Project Management
  - Source Code Management (SCM)
  - Continuous Integration and Continuous Delivery (CI/CD)
  - Value Stream Management (VSM)
  - Application Security (AppDevSec)
  - Release Automation
  - Continuous Deployment and Release Automation (CDRA)
  
  
  

## GitLab evaluated in recent reports

Here are links to recent analyst reports where GitLab the company and/or the product has been covered or evaluated. GitLab generally has rights to distribute these reports. Clicking these links will direct you to a page where you may download the report.

  - [The Forrester Wave™: Continuous Integration Tools, Q3 2017](https://reprints.forrester.com/#/assets/2/921/RES137261/reports)
  - [The Forrester New Wave™: Value Stream Management Tools, Q3 2018](https://reprints.forrester.com/#/assets/2/921/RES141538/reports)
  - IDC Innovators: Agile Code Development Technologies 2018   

## GitLab mentioned in recent reports

Here are links to reports where GitLab is mentioned within the context of the report. GitLab usually does not have rights to distribute these reports, which means you will likely have access to them only if you have a subscription to that analyst's reports through your own organization. Clicking these links will bring you to the report on that analyst company's web site, and you will need to log in to download or view the report.

### Forrester reports mentioning GitLab

  - [Evolve Or Retire: Administrators Are Now Developers, July 13, 2018](https://www.forrester.com/report/Evolve+Or+Retire+Administrators+Are+Now+Developers/-/E-RES137184)
  - [Elevate Agile-Plus-DevOps With Value Stream Management, May 11, 2018](https://www.forrester.com/report/Elevate+AgilePlusDevOps+With+Value+Stream+Management/-/E-RES142463)
  - [The State of Application Security, 2018, January 23, 2108](https://www.forrester.com/report/The+State+Of+Application+Security+2018/-/E-RES141676)
  - [The Quest For Speed-Plus-Quality Drives Agile And DevOps Tool Selection, April 17, 2017](https://www.forrester.com/report/The+Quest+For+SpeedPlusQuality+Drives+Agile+And+DevOps+Tool+Selection/-/E-RES122555)
  - [Cloud-Based DevOps Tools Are Ripening Quickly, April 4, 2017](https://www.forrester.com/report/CloudBased+DevOps+Tools+Are+Ripening+Quickly/-/E-RES137388)

### Gartner reports mentioning GitLab

 - [Hack Your Culture to Drive Quality and DevOps Success, 9 August, 2018](https://www.gartner.com/document/3885963?ref=solrAll&refval=208989697&qid=276917aca8f1fd77467886cbc7621b4bhttps://www.gartner.com/document/3885963?ref=solrAll&refval=208989697&qid=276917aca8f1fd77467886cbc7621b4b)
 - [Cool Vendors in DevOps, First Wave, 18 April, 2018](https://www.gartner.com/document/3872274?ref=solrAll&refval=208989697&qid=276917aca8f1fd77467886cbc7621b4b)
 - [Hype Cycle for I&O Automation 2018, 23 July 2018](https://www.gartner.com/document/3883474?ref=solrAll&refval=208989697&qid=276917aca8f1fd77467886cbc7621b4b)
 - [Hype Cycle for DevOps 2018, 24 July 2018](https://www.gartner.com/document/3883693?ref=solrAll&refval=208989697&qid=276917aca8f1fd77467886cbc7621b4b)
 - [Critical Capabilities for Application Security Testing, 19 March 2018](https://www.gartner.com/document/3869070?ref=solrAll&refval=208989697&qid=276917aca8f1fd77467886cbc7621b4b)
 - [Jump-Start Network Automation to Scale Digital Initiatives, 22 June 2018](https://www.gartner.com/document/3879870?ref=solrAll&refval=208989697&qid=276917aca8f1fd77467886cbc7621b4b)

### IDC reports mentioning GitLab

 - [Microsoft Significantly Expands Its Developer Collaboration Portfolio with GitHub Acquisition, June 5, 2018](https://www.idc.com/getdoc.jsp?containerId=lcUS43961318&pageType=PRINTFRIENDLY)
 - [IDC Market Glance: DevOps, 3Q18](https://www.idc.com/getdoc.jsp?containerId=US44268818)
 - [Google Cloud Next '18: Google Addresses Developer Needs with Developer Tools That Enhance Development in Cloud and On-Premises Environments, August 2, 2018](https://www.idc.com/getdoc.jsp?containerId=lcUS44193518&pageType=PRINTFRIENDLY)
 - [IDC MarketScape: Worldwide Cloud Testing and ASQ SaaS 2017-2018 Vendor Assessment — Enabling Quality in and on the Cloud, February 2018](https://www.idc.com/getdoc.jsp?containerId=US41601017)


## GitLab open issues
