---
layout: markdown_page
title: Secure Vision
---

## On this page
{:.no_toc}

- TOC
{:toc}

## DevOps stages coverage

The classic DevOps includes security testing as an activity in the **Verify** stage.

GitLab has a broader approach. That's why we introduced a **Secure** stage that is transversal to all other stages.

Our [Product Vision](https://about.gitlab.com/direction/product-vision/) for 2019 aims to provide security support for all the stages in the [DevOps toolchain](https://en.wikipedia.org/wiki/DevOps_toolchain): **Plan**, **Create**, **Verify**, **Package**, **Release**, **Configure**, and **Monitor**.

This is possible because GitLab is a [single application](https://about.gitlab.com/handbook/product/single-application/).

## Security Paradigm

GitLab security features support users in prioritizing, managing, and solving any security issue that may affect their environment. The primary focus is to increase awareness on security (onboarding) and to provide all the information needed to take decisions about that.

The approach is to _support_ decision makers, not to replace them. Instead of enforcing security, we want to give a very simple way to take the right action, and learn from it. Keeping it simple is a key value to prevent that security features will not be considered at all because they require more effort than the perceived benefit.

That's why security features are not supposed to automatically block a pipeline or to prevent a new version to be released on production. Even if we try to be accurate, results may suffer of false positives by their nature. Risk assessment will be mostly a human process.

Tools are actionable: it means that users can [interact with them and provide feedback](https://docs.gitlab.com/ee/user/project/merge_requests/#interacting-with-security-reports) about their content. When triaging vulnerabilities users can confirm them (creating an issue to solve the problem), or just dismiss them in case they are false positives and there is no further action to take. This information will be collected to improve the signal-to-noise ratio that the tool could provide in future executions.

They also need to be easy to use, and require the minimum amount of effort from users. If not, they will likely be disabled or not considered at all, missing their primary goal. Imagine if users had to explain why they are marking an email as spam every time!

## Target audience

### Security teams

We want to support security teams as first class citizens. GitLab should be their primary tool to manage monitoring and remediation of security issues. Using the **Security Dashboard** security specialists know exactly which is the most important thing they need to take care of, while Directors of Security can manage workflows and analyze historical data to figure out how to improve the response time.

This is a vulnerability-centric approach where items are grouped and ordered to suggest what's most important in a group, or in the entire instance.

### Developers

Nonetheless, we want to support developers and provide feedback during the application development. [**Security Reports**](https://docs.gitlab.com/ee/user/project/merge_requests/#security-reports) in merge request widgets and pipelines allow early access to security information that can be used to fix problems even before they are merged into the stable branch or released to the public, embrancing the idea of [shift left testing](https://en.wikipedia.org/wiki/Shift_left_testing).

This approach is valuable to highlight how specific changes could affect the security of the application.

## Impact

We expect that in most of the cases the number of potential security issues will be high, and we don't want that users will struggle in figuring out which is the impact of any possible item for their environment. That's why GitLab will prioritize security vulnerabilities based on different factors. The value given with this process is defined as the impact.

These are examples of factors that may contribute to define the impact for a specific security issue:
- severity and confidence levels, provided by the analysis tool
- feedback given in other reports for the same vulnerability
- exposure of the vulnerability (e.g., app has already been deployed to production)

## Availability

### Ultimate/Gold subscribers

As for now, security features are available in the [Ultimate/Gold tier](https://about.gitlab.com/pricing/) because we think that security matters for everyone, but automation of security in the development lifecycle is more valuable for organizations who want to optimize their DevOps investment to run securely at the highest business velocity.

### Public projects on GitLab.com

Every project on [GitLab.com](https://gitlab.com) with [public visibility](https://docs.gitlab.com/ee/public_access/public_access.html#public-projects) can benefit of all the security features for free.

## Metrics

We also want to collect and provide metrics to better understand how the security workflow is performing. For example, the overall time taken to deploy a fix for a vulnerability once spotted could be useful for flow improvements.
