---
layout: markdown_page
title: "GitLab Direction"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Product Vision

There is a separate page for our [Product Vision](product-vision) for GitLab at the end of 2018.

## Structure

This page describes the direction and roadmap for GitLab. It's organized from
the short to the long term.

## Your contributions

GitLab's direction is determined by GitLab the company, and the code that is
sent by our [contributors](http://contributors.gitlab.com/). We continually
merge code to be released in the next version. Contributing is the best way to
get a feature you want included.

On [our issue tracker for CE][ce-issues] and [EE][ee-issues], many requests are
made for features and changes to GitLab. Issues with the [Accepting Merge
Requests] label are pre-approved as something we're willing to add to GitLab. Of
course, before any code is merged it still has to meet our [contribution
acceptance criteria].

[ce-issues]: https://gitlab.com/gitlab-org/gitlab-ce/issues
[ee-issues]: https://gitlab.com/gitlab-org/gitlab-ee/issues
[Accepting Merge Requests]: https://gitlab.com/gitlab-org/gitlab-ce/issues?state=opened&label_name=Accepting+Merge+Requests
[contribution acceptance criteria]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria

## What our customers want

See our [product handbook on how we prioritize](/handbook/product/#prioritization).

## Roadmaps by stage

View the roadmaps by stage:

- [Manage](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amanage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS)
- [Plan](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aplan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS)
- [Create](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Acreate&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS)
- [Verify](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Averify&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS)
- [Package](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Apackage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS)
- [Release](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Arelease&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS)
- [Configure](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aconfigure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS)
- [Monitor](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amonitor&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS)
- [Secure](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Asecure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS)

## Previous releases

On our [releases page](/releases/) you can find an overview of the most
important features of recent releases and links to the blog posts for each
release.

## Future releases

GitLab releases a new version [every single month on the 22nd]. Note that we
often move things around, do things that are not listed, and cancel things that
_are_ listed.

This page is always in draft, meaning some of the things here might not ever be
in GitLab. New Starter, Premium, and Ultimate features are indicated with
labels. This is our best estimate of where new features will land, but is in no
way definitive.

The list is an outline of **tentpole features** -- the most important features
of upcoming releases -- and doesn't include most contributions from volunteers
outside the company. This is not an authoritative list of upcoming releases - it
only reflects current [milestones](https://gitlab.com/groups/gitlab-org/milestones).

### Releases

<%= direction %>

## Enterprise Editions

### Starter

Starter features are available to anyone with an Enterprise Edition subscription (Starter, Premium, Ultimate).

<%= wishlist["GitLab Starter"] %>

### Premium

Premium features are only available to Premium (and Ultimate) subscribers.

<%= wishlist["GitLab Premium"] %>

### Ultimate

Ultimate is for organisations that have a need to build secure, compliant
software and that want to gain visibility of - and be able to influence - their
entire organisation from a high level. Ultimate features are only be available
to Ultimate subscribers.

<%= wishlist["GitLab Ultimate"] %>

[every single month on the 22nd]: /2015/12/07/why-we-shift-objectives-and-not-release-dates-at-gitlab

## DevOps Stages

Grouping is based on stages in the [DevOps
lifecycle](https://en.wikipedia.org/wiki/DevOps_toolchain) and our [product
categories](/handbook/product/categories/).

![DevOps Lifecycle](product-vision/devops-loop-and-spans.png)

![Product Categories](product-vision/product-categories.png)

These features represent the various functional areas we see GitLab going in.
This list is not prioritized. We invite everyone to join the discussion by
clicking on the items that are of interest to you. Feel free to comment, vote up
or down any issue or just follow the conversation. For GitLab sales, please add
a link to the account in Salesforce.com that has expressed interest in a
feature. We very much welcome contributions that implement any of these things.

### Manage

#### Moderation Tools

<%= wishlist["moderation"] %>

### Plan

See the [1-year roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Plan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS) for the [Plan stage](/handbook/product/categories/#plan) of the DevOps lifecycle.

#### Portfolio management with epics and roadmaps

GitLab portfolio management (together with project management below) allows different team members
and stakeholders throughout your organization to innovate and collaborate on high-level ideas;
turn those into strategic initiatives with executive sponsorship; scope and estimate work effort required;
plan that work across quarters and years; and execute it in weeks and months.

- [Sorting in epics list view and roadmap view](https://gitlab.com/groups/gitlab-org/-/epics/236)
- [Epic email notifications, todos, and autocomplete](https://gitlab.com/groups/gitlab-org/-/epics/148)
- [Close epics](https://gitlab.com/groups/gitlab-org/-/epics/145)
- [Epics and roadmaps integrated with milestones](https://gitlab.com/groups/gitlab-org/-/epics/227)
- [Make epics easier to use](https://gitlab.com/groups/gitlab-org/-/epics/134)
- [Moving forward and backward in time in roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/311)
- [Flexible work breakdown structure with epic relationships](https://gitlab.com/groups/gitlab-org/-/epics/312)
- [Inherit children epics start and due dates](https://gitlab.com/groups/gitlab-org/-/epics/318)
- [Weight and progress information in epics and roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/76)
- [Autoclose epic](https://gitlab.com/groups/gitlab-org/-/epics/327)
- [Milestones in roadmap](https://gitlab.com/groups/gitlab-org/-/epics/329)
- [Searching in epics](https://gitlab.com/groups/gitlab-org/-/epics/315)
- [Polish epic relationships](https://gitlab.com/groups/gitlab-org/-/epics/317)

#### Project management with issues and boards

- [Managing many boards](https://gitlab.com/groups/gitlab-org/-/epics/336)
- [Filtering options in system activity](https://gitlab.com/groups/gitlab-org/-/epics/347)
- [Track issue and merge request description changes](https://gitlab.com/groups/gitlab-org/-/epics/237)
- [Resolvable discussions in issues and epics](https://gitlab.com/groups/gitlab-org/-/epics/226)
- [`not` and `or` in search bars](https://gitlab.com/groups/gitlab-org/-/epics/291)
- [Multiple milestones per issue or merge request](https://gitlab.com/groups/gitlab-org/-/epics/69)
- [Group milestones parity with project milestones](https://gitlab.com/groups/gitlab-org/-/epics/6)
- [Filter by project on group lists and group board](https://gitlab.com/groups/gitlab-org/-/epics/330)
- [Move default description templates out of project settings](https://gitlab.com/groups/gitlab-org/-/epics/238)
- [Bulk manage issue and merge request subscriptions](https://gitlab.com/groups/gitlab-org/-/epics/156)
- [Move default description templates out of project settings](https://gitlab.com/groups/gitlab-org/-/epics/238)
- [Milestone change events for system notes](https://gitlab.com/groups/gitlab-org/-/epics/238)
- [Advanced integrated boards for teams](https://gitlab.com/groups/gitlab-org/-/epics/274)
- [Bulk edit at group level](https://gitlab.com/groups/gitlab-org/-/epics/310)

#### Personal workflow management with notifications and todos

GitLab helps you get your work done, personalized to your own specific workflows,
using notifications and todos.

- [Unified todos and notifications](https://gitlab.com/groups/gitlab-org/-/epics/149)

#### Governance with enforced workflows and custom fields

Larger enterprises require enhanced governance in their business management and
software delivery, which can be addresed with enforced workflows and custom fields.

- [Enforced workflows](https://gitlab.com/groups/gitlab-org/-/epics/364)
- [Custom fields](https://gitlab.com/groups/gitlab-org/-/epics/235)

#### Analytics

GitLab has in-product analytics, helping teams track performance over time,
highligting major problems, and providing the actionable insight to solve them.

- [Burndown charts in boards](https://gitlab.com/groups/gitlab-org/-/epics/230)
- [Analytics chart dashboards](https://gitlab.com/groups/gitlab-org/-/epics/313)
- [VSM analytics](https://gitlab.com/groups/gitlab-org/-/epics/228)
- [VSM analytics standalone and also integrated into boards](https://gitlab.com/groups/gitlab-org/-/epics/229)
- [Advanced analytics in boards for teams](https://gitlab.com/groups/gitlab-org/-/epics/233)

#### Search

Teams leverage GitLab search to quickly search for relevant content, enabling stronger
intra-team and cross-team collaboration through discovery of all GitLab data.

- [Get Elasticsearch working on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/153)
- [Support Elasticsearch beyond Global search](https://gitlab.com/groups/gitlab-org/-/epics/154)

#### Jira integration

GitLab supports deep Jira integration, allowing teams to use Jira for issue mangement, but still
leveraging the benefits of GitLab source control and other native GitLab features.

- [Import Jira issues to GitLab issues](https://gitlab.com/groups/gitlab-org/-/epics/10)
- [Better than Atlassian Jira integration](https://gitlab.com/gitlab-org/gitlab-ce/issues/27073)

### Create

GitLab is used to create, collaborate, review and manage content; often source
code, by many businesses. We want to make it possible for everyone to be able
to contribute content and feedback using GitLab.

#### Source code management

Inner sourcing, collaboration across organizational boundaries and contibuting
patches to upstream open source projects means embracing the benefits of Git
being a tool built for distributed software development. GitLab helps teams
collaboratively write software, and will make it easier to collaborate across
organizational and server boundaries.

<%= wishlist["repository"] %>

#### Merge Requests

A key part of writing and deploying high quality code is a thorough code review
approval process. Merge requests provide this and are integrated with GitLab CI
for testing, security testing, monitoring and more.

Providing the tools for great code reviews improves code quality and helps
teams iterate faster. We will help teams move faster by supporting more
sophisticated commit, review, and merge strategies beyond treating a merge
request as a single patch.

<%= wishlist["merge requests"] %>
<%= wishlist["code review"] %>
<%= wishlist["approvals"] %>

#### Web IDE

<%= wishlist["web ide"] %>

#### Wiki

<%= wishlist["wiki"] %>

#### Snippets

<%= wishlist["snippets"] %>

#### Version Control for Everything

<%= wishlist["vcs for everything"] %>

#### Large files

Git was primarily designed for code, where it is dominating the world.
Organisations working with large files, for instance art assets when creating
films or games, are looking to get the power that GitLab offers, but can't adopt because of
the little support for large files that Git offers.

GitLab is going to help these organisations adopt Git and migrate away from legacy platforms such
as Perforce. There are a number of moving parts that make this possible:

##### Git LFS

Git LFS allows you to work well with large files in Git.

<%= wishlist["lfs"] %>

##### File locking

File locking is crucial to collaborating on non-mergeable files.
Currently GitLab offers branch-limited file locking and Git LFS offers
locking across branches.

<%= wishlist["file locking"] %>

### Verify

The Verify stage of the DevOps pipeline covers the CI lifecycle as well as
testing (unit, integration, acceptance, performance, etc.) Our mission is to
help developers feel confident in delivering their code to production.

The complete vision for Verify can be found [here](/direction/verify).

### Package

GitLab is the engine that powers many companies' software businesses so it is
important to ensure it is as easy as possible to deploy, maintain, and stay up
to date.

Today we have a mature and easy to use Omnibus based build system, which is the
foundation for nearly all methods of deploying GitLab. It includes everything a
customer needs to run GitLab all in a single package, and is great for
installing on virtual machines or real hardware. We are committed to making our
package easier to work with, highly available, as well as offering automated
deployments on cloud providers like AWS.

We also want GitLab to be the best cloud native development tool, and offering a
great cloud native deployment is a key part of that. We are focused on offering
a flexible and scalable container based deployment on Kubernetes, by using
enterprise grade Helm Charts.

#### GitLab High Availability

<%= wishlist["HA"] %>

#### Container Registry

<%= wishlist["container registry"] %>

### Release

The Release stage of the DevOps pipeline covers automated, repeatable, and
risk-reduction for releases, such as CD and feature flags, as well as the pages
feature. We want to make delivering software reliable, repeatable, and
successful.

The complete vision for Release can be found [here](/direction/release).

### Configure

#### Application Control Panel

We define our vision as “[Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c)”:
leveraging our [single application](/handbook/product/single-application/), it
is simple to assist users in every phase of the development process,
implementing automatic tasks that can be customized and refined to get the best
fit for their needs. Our idea is that the future will have “auto CI” to compile
and test software based on best practices for the most common languages and
frameworks, “auto review” with the help of automatic analysis tools like Code
Climate, “auto deploy” based on Review Apps and incremental rollouts on
Kubernetes clusters, and “auto metrics” to collect statistical data from all the
previous steps in order to guarantee performances and optimization of the whole
process. Dependencies and artifacts will be first-class citizens in this world:
everything must be fully reproducible at any given time, and fully connected as
part of the great GitLab experience.

[Watch the video explaining our vision on Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c).

### Monitor

Performance is a critical aspect of the user experience, and ensuring your application is responsive and available is everyone's responsibility. We want to help address this need for development teams, by integrating key performance analytics and feedback into the tool developers already use every day.

As part of our commitment to performance we are also deeply instrumenting GitLab itself, enabling our team to improve GitLab peformance and for customers to more easily manage their deployments.

<%= wishlist["Monitoring"] %>

### Secure

You can find all the information in the [Secure Vision](/direction/secure/) page.

## Moonshots

Moonshots are big hairy audacious goals that may take a long time to deliver.

<%= wishlist["moonshots"] %>

## Usability

<%= wishlist["usability"] %>

## Open Source Projects

<%= wishlist["open source"] %>

## Performance

<%= wishlist["performance"] %>

## Scope

[Our vision](#vision) is to replace disparate DevOps toolchains with a single integrated application that is pre-configured to work by default across the complete DevOps lifecycle. Inside our scope are the [9 stages of the DevOps lifecycle](/stages-devops-lifecycle/). Consider viewing [the presentation of our plan for 2018](/2017/10/11/from-dev-to-devops/).

We try to prevent maintaining functionality that is language or platform specific because they slow down our ability to get results. Examples of how we handle it instead are:

1. We don't make native mobile clients, we make sure our mobile web pages are great.
1. We don't make native clients for desktop operating systems, people can use [Tower](https://www.git-tower.com/mac/) and for example GitLab was the first to have merge conflict resolution in our web applications.
1. For language translations we [rely on the wider community](https://docs.gitlab.com/ee/development/i18n/translation.html).
1. For Static Application Security Testing we rely on [open source security scanners](https://docs.gitlab.com/ee/ci/examples/sast.html#supported-languages-and-frameworks).
1. For code navigation we're hesitant to introduce navigation improvements that only work for a subset of languages.
1. For [code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality_diff.html) we reuse Codeclimate Engines.
1. For building and testing with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) we use Heroku Buildpacks.

Outside our scope are:

1. **Network** (fabric) [Flannel](https://github.com/coreos/flannel/), Openflow, VMware NSX, Cisco ACI
1. **Proxy** (layer 7) [Envoy](https://envoyproxy.github.io/), [nginx](https://nginx.org/en/), [HAProxy](http://www.haproxy.org/), [traefik](https://traefik.io/)
1. **Ingress** [(north/south)](https://networkengineering.stackexchange.com/a/18877) [Contour](https://github.com/heptio/contour), [Ambassador](https://www.getambassador.io/),
1. **Service mesh** [(east/west)](https://networkengineering.stackexchange.com/a/18877) [Istio](https://istio.io/), [Linkerd](https://linkerd.io/)
1. **Container Scheduler** we mainly focus on Kubernetes, other container schedulers are: CloudFoundry, OpenStack, OpenShift, Mesos DCOS, Docker Swarm, Atlas/Terraform, [Nomad](https://nomadproject.io/), [Deis](http://deis.io/), [Convox](http://www.convox.com/), [Flynn](https://flynn.io/), [Tutum](https://www.tutum.co/), [GiantSwarm](https://giantswarm.io/), [Rancher](https://github.com/rancher/rancher/blob/master/README.md)
1. **Package manager** [Helm](https://github.com/kubernetes/helm), [ksonnet](http://ksonnet.heptio.com/)
1. **Operating System** Ubuntu, CentOS, [RHEL](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux), [CoreOS](https://coreos.com/), [Alpine Linux](https://alpinelinux.org/about/)

## Product Strategy

Our goal is to make it faster and easier for groups of people (organisations, open source projects) to
bring value to their customers. This is measureable:

- Faster: cycle time
- Easier: amount of steps to go from an idea to having it fully available (in production, monitored)

We're able to do this by providing a [_single application_](/handbook/product/single-application/) that [_plays well with others_](#plays-well-with-others) for [_teams of any size_](#teams-of-any-size) with [_any kind of projects_](#any-project), while giving you [_actionable feedback_](#actionable-feedback).

## Actionable Feedback

Deployments should never be fire and forget. GitLab will give you immediate
feedback on every deployment on any scale. This means that GitLab can tell you
whether performance has improved on the application level, but also whether
business metrics have changed.

Concretely, we can split up monitoring and feedback efforts within GitLab in
three distinct areas: execution (cycle analytics), business and system feedback.

### Business feedback

With the power of monitoring and an integrated approach, we have the ability to
do amazing things within GitLab. GitLab will be able to automatically test
commits and versions through feature flags and A/B testing.

Business feedback exists on different levels:

* Short term: how does a certain change perform? Choose A/B based on data.
* Medium term: did a particular new feature change conversions, engagement
* Long term: how do larger efforts relate to changes in conversations, engagement, revenue

- [A/B Testing of branches](https://gitlab.com/gitlab-org/gitlab-ee/issues/117)

### Application feedback

You application should perform well after changes are made. GitLab will be able to
see whether a change is causing errors or performance issues on application level.
Think about:

* Response times of e.g. a backend API
* Error rates and occurrences of new bugs
* Changes in API calls

### System feedback

We can now go beyond CI and CD. GitLab will able to tell you whether a change
improved performance or stability. Because it will have access to both
historical data on performance and code, it can show you the impact of any
particular change at any time.

System feedback happens over different time windows:

* Immediate: see whether changes influence availability and alert if they do
* Short-medium term: see whether changes influence system metrics and performance
* Medium-Long term: did a particular effort influence system status

- Implemented: [Performance Monitoring](https://docs.gitlab.com/ee/administration/monitoring/performance/introduction.html)
- [Status monitoring and feedback](https://gitlab.com/gitlab-org/gitlab-ce/issues/25555)
- [Feature monitoring](https://gitlab.com/gitlab-org/gitlab-ce/issues/24254)

### Execution Feedback & Cycle Analytics

GitLab is able to speed up cycle time for any project.
To provide feedback on cycle time GitLab will continue to expand cycle
analytics so that it not only shows you what is slow, it’ll help you speed up
with concrete, clickable suggestions.

- [Cycle Speed Suggestions](https://gitlab.com/gitlab-org/gitlab-ce/issues/25281)

## Why cycle time is important

The ability to monitor, visualize and improve upon cycle time (or: time to value) is fundamental
to GitLab's product. A shorter cycle time will allow you to:

- respond to changing needs faster (i.e. skate to where the puck is going to be)
- ship smaller changes
- manage regressions, rollbacks, bugs better, because you're shipping smaller changes
- make more accurate predictions
- focus on improving customer experience, because you're able to respond to their needs faster

When we're adding new capabilities to GitLab, we tend to focus on things that
will reduce the cycle time for our customers. This is why we choose
[convention over configuration](/handbook/product/#convention-over-configuration)
and why we focus on automating the entire software development lifecycle.

All friction of setting up a new project and building the pipeline of tools
you need to ship any kind of software should disappear when using GitLab.

## Plays well with others

We understand that not everyone will use GitLab for everything all the time, especially when first adopting GitLab.
We want you to use more of GitLab because you love that part of GitLab.
GitLab plays well with others, even when you use only one part of GitLab it should be a great experience.

GitLab ships with built-in integrations to many popular applications. We aspire to have the worlds best integrations for Slack, JIRA, and Jenkins.

Many other applications [integrate with GitLab](/integrations/), and we are open to adding new integrations to our [applications page](/applications/). New integrations with GitLab can very in richness and complexity; from a simple webhook, and all the way to a [Project Service](https://docs.gitlab.com/ee/user/project/integrations/project_services.html).

GitLab [welcomes and supports new integrations](/integrations/) to be created to extend collaborations with other products.
GitLab plays well with others by providing APIs for nearly anything you can do within GitLab.
GitLab can be a [provider of authentication](https://docs.gitlab.com/ee/integration/oauth_provider.html) for external applications.
And of course GitLab is open source so people are very welcome to add anything that they are missing.
If you are don't have time to contribute and am a customer we gladly work with you to design the API addition or integration you need.

## Enterprise Editions

GitLab comes in 4 editions:
* **Core**: This edition is aimed at solo developers or teams that
do not need advanced enterprise features. It contains a complete stack with all
the tools developers needs to ship software.
* **Starter**: This edition contains features that are more
relevant for organizations that have more than 100 potential users. For example:
	* features for managers (reports, management tools at the group level,...),
	* features targeted at developers that have to work in multi-disciplinary
	teams (merge request approvals,...),
	* integrations with external tools.
* **Premium**: This edition contains features that are more
relevant for organizations that have more than 750 potential users. For example:
	* features for instance administrators
	* features for managers at the instance level (reporting, management tools,
	roles,...)
	* features to help teams that are spread around the world
	* features for people other than developers that help ship software (support,
	QA, legal,...)
* **Ultimate**: This edition contains
features that are more relevant for organizations that have more than 5000
potential users. For example:
	* compliances and certifications,
	* change management.

## ML/AI at GitLab

Machine learning (ML) through neural networks is a really great tool to solve hard to define, dynamic problems.
Right now, GitLab doesn't use any machine learning technologies, but we expect to use them in the near future
for several types of problems:

### Signal / noise separation

Signal detection is very hard in an noisy environment. GitLab plans to use
ML to warn users of any signals that stand out against the background noise in several features:

- security scans, notifying the user of stand-out warnings or changes
- error rates and log output, allowing you to rollback / automatically rollback a change if the network notices abberant behavior

### Recommendation engines

Automatically categorizing and labelling is risky. Modern models tend to overfit, e.g. resulting
in issues with too many labels. However, similar models can be used very well in combination
with human interaction in the form of recommendation engines.

- [suggest labels to add to an issue / MR (one click to add)](https://gitlab.com/tromika/gitlab-issues-label-classification)
- suggest a comment based on your behavior
- suggesting approvers for particular code

### Smart behavior

Because of their great ability to recognize patterns, neural networks are an excellent
tool to help with scaling, and anticipating needs. In GitLab, we can imagine:

- auto scaling applications / CI based on past load performance
- prioritizing parallized builds based on the content of a change

### Code quality

Similar to [DeepScan](https://deepscan.io/home/).

### Code navigation

Similar to [Sourcegraph](https://about.sourcegraph.com/).

## Quarterly Objectives and Key Results (OKRs)

To make sure our goals are clearly defined and aligned throughout the organization, we make use of OKR's (Objective Key Results). Our [quarterly Objectives and Key Results](/okrs) are publicly viewable.

## Vision

From development teams to marketing organizations, everyone needs to collaborate on
digital content. Content should be open to suggestions by a wide number of
potential contributors. Open contribution can be achieved by using a mergeable
file format and distributed version control. The vision of GitLab is to **allow
everyone to collaborate on all digital content** so people can cooperate
effectively and achieve better results, faster.

Ideas flow though many stages before they are realized. An idea originates in a chat discussion, an issue is created, it is planned in a sprint, coded in an IDE, committed to
version control, tested by CI, code reviewed, deployed, monitored, and documented. Stitching all these stages of the DevOps lifecycle together can be done in many different ways. You can have a marketplace of
proprietary apps from different suppliers or use a suite of products developed
in isolation. We believe that a **single application for the DevOps lifecycle based on convention over configuration** offers a superior user experience. The
advantage can be quoted from the [Wikipedia page for convention over
configuration](https://en.wikipedia.org/wiki/Convention_over_configuration):
"decrease the number of decisions that developers need to make, gaining
simplicity, and not necessarily losing flexibility". In GitLab you only have to
specify unconventional aspects of your workflow.
The happy path is **frictionless from planning to monitoring**.

We admire other convention over configuration tools like [Ruby on
Rails](http://rubyonrails.org/) (that doctrine of which perfectly describe the [value of integrated systems](http://rubyonrails.org/doctrine#integrated-systems)), [Ember](http://emberjs.com/), and
[Heroku](https://www.heroku.com/), and strive to offer the same advantages for a
continuous delivery of software.
