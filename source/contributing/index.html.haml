---
layout: default
title: Contributing
description: "Learn about contributing to GitLab and read the guidelines, contribution process, sponsor the development of new features, and more."
suppress_header: true
extra_css:
  - contributing.css
---
.wrapper
  .simple-header
    %h1 Contributing to GitLab
  #content
    .wrapper.container.contributing-content{role: "main"}
      .block
        .block-content
          %h2.block-title Introduction
          :markdown
            We want to make it as easy as possible for GitLab users to become GitLab contributors, so we’ve created this guide to help you get started. We have multiple tracks to cater to people of varying experience levels.
        
            If you’re uncomfortable getting into open source development right away, you may want to consider the Documentation or Translation tracks. Documentation and Translation are both just as important as code, and we'd be happy to have your contributions.

      .block
        .block-content
          %h2.block-title Code of Conduct
          :markdown
            As contributors and maintainers of this project, we pledge to respect all people who contribute through reporting issues, posting feature requests, updating documentation, submitting pull requests or patches, and other activities.

            We are committed to making participation in this project a harassment-free experience for everyone, regardless of level of experience, gender, gender identity and expression, sexual orientation, disability, personal appearance, body size, race, ethnicity, age, or religion.

            Examples of unacceptable behavior by participants include the use of sexual language or imagery, derogatory comments or personal attacks, trolling, public or private harassment, insults, or other unprofessional conduct.

            Project maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct. Project maintainers who do not follow the Code of Conduct may be removed from the project team.

            This code of conduct applies both within project spaces and in public spaces when an individual is representing the project or its community.

            Instances of abusive, harassing, or otherwise unacceptable behavior can be reported by emailing `contact@gitlab.com`.

            This Code of Conduct is adapted from the [Contributor Covenant](http://contributor-covenant.org/) version 1.1.0, available at <http://contributor-covenant.org/version/1/1/0/>.

      .block
        .block-content
          %h2.block-title Development
          :markdown
            These instructions are for development of GitLab Community Edition specifically. Please note that use of the GitLab Development Kit is currently experimental on Windows. macOS or Linux are recommended for the best contribution experience.

            1. Download and set up the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit), see the [GDK README](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/README.md) for instructions on setting it up and [Troubleshooting](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/troubleshooting.md) if you get stuck.
            1. Fork the GitLab CE project.
            1. Choose an issue to work on.
                - You can find easy issues by [looking at issues in the Backlog (Accepting merge requests) milestone and sorting issues by weight](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=✓&state=opened&assignee_id=0&milestone_title=Backlog%20(Accepting%20merge%20requests)), low-weight issues will be the easiest to accomplish.
                - Be sure to comment and verify no one else is working on the issue, and to make sure we’re still interested in a given contribution.
                - You may also want to comment and ask for help if you’re new or if you get stuck. We’re more than happy to help!
            1. Add the feature or fix the bug you’ve chosen to work on.
            1. Open a merge request. The earlier you open a merge request, the sooner you can get feedback. You can mark it as a Work in Progress to signal that you’re not done yet.
            1. Add tests and documentation if needed, as well as [a changelog entry](https://docs.gitlab.com/ee/development/changelog.html).
            1. Make sure the test suite is passing.
            1. Wait for a reviewer. You’ll likely need to change some things once the reviewer has completed a code review for your merge request. You may also need multiple reviews depending on the size of the change.
            1. Get your changes merged!

            For more information, please see the [Developer Documentation](https://docs.gitlab.com/ee/development/README.html).

      .block
        .block-content
          %h2.block-title Documentation
          :markdown
            See the [Documentation Styleguide](https://docs.gitlab.com/ee/development/doc_styleguide.html) and [Writing Documentation](https://docs.gitlab.com/ee/development/writing_documentation.html) pages for important information on writing documentation for GitLab.

            1. Visit [docs.gitlab.com](https://docs.gitlab.com) for the latest documentation for GitLab CE/EE, GitLab Runner, and GitLab Omnibus.
            1. Find a page that’s lacking important information or that has a spelling/grammar mistake.
            1. Click the "Edit this page" link at the bottom of the page.
              * Note that this will most likely link you to the GitLab EE repository, most features are available in GitLab CE and you'll likely want to contribute to CE instead. Simply replace `gitlab-ee` in the URL with `gitlab-ce`, e.g. `https://gitlab.com/gitlab-org/gitlab-ee/blob/master/doc/administration/container_registry.md` would become `https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/administration/container_registry.md`.
            1. Fork the relevant project and modify the documentation in GitLab’s web editor.
            1. Open a merge request.
            1. Follow [branch naming conventions](https://docs.gitlab.com/ee/development/writing_documentation.html#branch-naming) and append `-docs` to the name of the branch.
            1. Wait for a review, you may need to change things if a reviewer requests it.
            1. Get your changes merged!

            For those interested in writing full technical articles, we also have a [GitLab Community Writers Program](/community-writers/) which includes compensation for writing technical articles.

      .block
        .block-content
          %h2.block-title Translation
          :markdown
            Please note that GitLab is in the process of being internationalized. Not all pages have been updated to be translatable, and all languages other than English are incomplete. For more information visit [the documentation](https://docs.gitlab.com/ee/development/i18n/translation.html).

            1. Visit our [Crowdin project](https://translate.gitlab.com/) and sign up.
            1. Find a language you’d like to contribute to.
            1. Improve existing translations, vote on new translations, and/or contribute new translations to your given language.
            1. Once approved, your changes will automatically be included in the next version of GitLab!
      
      .block
        .block-content
          %h2.block-title UX Design
          :markdown
            These instructions are for those wanting to contribute UX designs specifically. The UX department at GitLab uses Sketch for all of its designs. See the [Design Repository documentation](https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md#getting-started) for details on working with our files. Visit our [Contributing guidelines](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/index.md) to read our general guidelines for contributing. While they are code-focused instructions, they will help you understand the overall process of contributing.

            1. If you want to leverage our existing design library to submit UX proposals, you can download our pattern library Sketch file following the instructions in the [Design Repository documentation](https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md#getting-started).
            1. You do not need to use our Sketch files to contribute. We will gladly accept hand-made drawings and sketches, wireframes, manipulated DOM screenshots, prototypes, etc.
            1. You can find documentation on our design patterns in our [Design System](https://design.gitlab.com/). Use it to understand where and when to use common design solutions.
            1. If you don't already have an idea for a UX improvement, you can pick an existing problem to work on from [this list of issues looking for community contributions](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Accepting%20Merge%20Requests&label_name[]=UX)
            1. Be sure to comment and verify no one else is working on the UX for the issue, and to make sure we’re still interested in a given contribution.
            1. Ask questions to clarify the problem being solved and make your UX suggestions using words, low-fi wireframes, hi-fi designs, or even prototypes. Ask for help if you’re new or if you get stuck. We’re happy to help! You can ping the UX Department in an issue or comment using this handle `@gitlab-com/gitlab-ux`.
            1. Ask for a review from a GitLab UX Designer. You’ll likely need to change some things once the reviewer has completed their review. You may also require multiple reviews depending on the scope of the UX.
            1. Get your UX approved!
