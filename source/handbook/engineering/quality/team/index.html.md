---
layout: markdown_page
title: "Quality Team"
---

## Onboarding

Copy the [Quality team onboarding issue template](https://gitlab.com/gitlab-org/quality/team-tasks/blob/master/.gitlab/issue_templates/Onboarding.md)
into a new issue in [QA Team Tasks](https://gitlab.com/gitlab-org/quality/team-tasks/issues/new)
and complete the issue.

## Links

- Gitlab QA
  - [Testing Guide / E2E Tests](https://docs.gitlab.com/ee/development/testing_guide/end_to_end_tests.html)
  - [GitLab QA Orchestrator Documentation](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/README.md)
  - [GitLab QA Testing Documentation](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/README.md)
- Tests
  - [Testing Standards](https://docs.gitlab.com/ee/development/testing_guide/index.html)
- CI infrastructure for CE and EE
  - [Testing from CI](https://docs.gitlab.com/ee/development/testing_guide/ci.html)
- Tests statistics
  - [Redash Test Suite Statistics](https://redash.gitlab.com/dashboard/test-suite-statistics)
- Insights dashboard
  - [Quality Dashboard](http://quality-dashboard.gitlap.com/)
  - [Quality Dashboard Documentation](https://gitlab.com/gitlab-org/gitlab-insights/blob/master/README.md)
- QA Runners
  - [QA Runner Ownership Issue](https://gitlab.com/gitlab-org/gitlab-qa/issues/261)
- Projects
  - [gitlab-org](https://gitlab.com/gitlab-org)
  - [gitlab-com](https://gitlab.com/gitlab-com)
  - [dev.gitlab.org](https://dev.gitlab.org)
  - [staging.gitlab.com](https://staging.gitlab.com)
