---
layout: markdown_page
title: Product categories
extra_js:
    - listjs.min.js
    - categories.js
---

:markdown
    ## Introduction

    We want intuitive interfaces both within the company and with the wider
    community. This makes it more efficient for everyone to contribute or to get
    a question answered. Therefore, the following interfaces are based on the
    product categories defined on this page:

    - [Product Vision](/direction/product-vision/)
    - [Direction](/direction/#devops-stages)
    - [Software Development Life-Cycle (SDLC)](/sdlc/#stacks)
    - [Home page](/)
    - [Product page](/product/)
    - [Product Features](/features/)
    - [Pricing page](/pricing/)
    - [Documentation](https://docs.gitlab.com/ee/#complete-devops-with-gitlab)
    - [Engineering](https://about.gitlab.com/handbook/engineering/) Engineering Manager/Developer/Designer titles, their expertise, and department, and team names.
    - [Product manager](https://about.gitlab.com/handbook/product/) responsibilities which are detailed on this page
    - [Our pitch deck](https://about.gitlab.com/handbook/marketing/product-marketing/#company-pitch-deck), the slides that we use to describe the company
    - [Product marketing](https://about.gitlab.com/handbook/marketing/product-marketing/) specializations

    ## Hierarchy

    The categories form a hierarchy:

    1. **Departments**: Dev and Ops. Dev and Ops map to departments in our [organization structure](https://about.gitlab.com/team/structure/#table). At GitLab the Dev and Ops split is different then the infinity loop suggests because our CI/CD functionality is one codebase, so from verify on we consider it Ops so that the codebase falls under one department.
    The stages that are the difference between the value stages and the team stages are part of the Dev department.
    1. **Stages**: Stages start with the 7 **loop stages**, then add Manage and Secure to give the 9 (DevOps) **value stages**, and then add Distribution, Geo, Gitaly & Gitter to get the 12 **team stages**. Values stages are what we all talk about in our marketing. Each of the team stages has a dedicated engineering team, product manager. Within shared functions like quality and product management individuals are paired to one or more stages so that there are stable counterparts.
    1. **Categories**: High-level capabilities that may be a standalone product at another company. e.g. Portfolio Management. Or the **internal customers** of the stage who practice our [collaborate value](https://about.gitlab.com/handbook/values/#collaboration) of dogfooding.
    1. **Capabilities**: An optional group of functionality that is made up of smaller discrete features. e.g. Epics. Capabilities are listed as the second tier of bullets below. We have [feature categories](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/feature_categories.yml) but it is incomplete and not used everywhere.
    1. **Features**: Small, discrete functionalities. e.g. Issue weights. Some common features are listed within parentheses to facilitate finding responsible PMs by keyword. Features are maintained in [features.yml](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml).

    Eventually, every category and capability listed on this page should link to a product marketing page, documentation, or an epic/issue.

    ## Changes

    The impact of changes to stages is felt [across the company](team/structure/#stage-groups).
    Merge requests with changes to stages and significant changes to categories need to be approved by:

    1. Head of Product
    1. VP of Product
    1. VP of Engineering
    1. [CEO](https://about.gitlab.com/team/structure/#layers)

    ## Value stages

    ![DevOps stages](/handbook/product/categories/devops-loop-and-spans.png)

    ### [Manage](/handbook/product/categories/manage)
    Organizations manage their operation to optimize their value to their users, customers, and stakeholders by continuously improving their products and services. As such, the business needs insight and data into their effectiveness and they need specific process, cycle, and financial metrics to improve their performance. The process of managing the business is always on.

    ### Plan
    Planning is focused on the prioritization of ideas, allocation of resources ($, human and technology), scheduling projects, tracking status, coordination across the business, and resolving conflicts between efforts. Typically planning includes practices such as: Collaboration, Issue Management, Project Management, Program Management, Portfolio Management, Resource Management, Requirements Management, and Financial Management (as it relates to projects).

    ### Create
    Collaboratively design, develop, and implement capabilities in the application to meet business goals and objectives. Teams establish processes and mechanisms to track and review multiple versions of their code as they iterate through frequent improvements.

    ### Verify
    Verification ensures the quality of the software by automatically and consistently building, integrating, and testing code changes; minimizing conflicts and rework. Verification includes functional testing (unit, integration, and acceptance) and non-functional testing (performance, security, and usability). Ideally, verification tests are fully automated and provide rapid feedback so that only high quality changes are accepted.

    ### Package
    Assemble and manage the different versions of components, libraries, and other elements that are required to run the application, so that the application can be deployed consistently and repeatedly to different execution environments.

    ### Release
    Deliver and deploy the application in the target environment. When 'releasing' the application, the environment is typically production. Often teams will use strategies to gradually release changes into their environment, such as Canary deployments or Blue/Green deployments.

    ### Configure
    Make application-specific settings that enable the application to be fully functional and optimized in a specific environment. Ideally configuration settings and details are centrally managed (like code) and are applied automatically to reduce the potential for human error.

    ### Monitor
    Rapid and reliable feedback from production is critical in order to embrace continuous improvement. This feedback must be collected from both external and internal vantage points to be effective. Applications must be designed and built to allow for telemetry and utilization data to enable the business and delivery team to iterate and continuously improve the application.

    ### Secure
    Ensuring the application is secure and trusted is an activity that spans the entire development lifecycle from planning to monitoring. Requirements and policies should set security expectations, design and development incorporate secure practices, every change should be tested for security (SAST,DAST), libraries and dependencies tracked and managed, containers and infrastructure secured, and the entire lifecycle monitored to ensure compliance. Delivering secure applications depends on every stage in the life cycle.

:markdown
    ## Dev department

    ### Dev leadership

    - Product: Job
    - Backend: Tommy
    - Product Marketing: Ashish

%h3 Dev stages
= partial "includes/product/engineering_area", locals: { dev_ops: 'dev'}

:markdown
    ## Ops department

    ### Ops leadership

    - Product: Mark
    - Backend: Dalia
    - Product Marketing: Ashish

%h3 Ops stages
= partial "includes/product/engineering_area", locals: { dev_ops: 'ops'}

:markdown
    ## Composed categories

    GitLab also does the things below that are composed of multiple categories.

    1. Software Composition Analysis (SCA) = Dependency Scanning + License Management
    1. Interactive Application Security Testing (IAST) = Dynamic application security testing (DAST) + Runtime Application Self-Protection (RASP)

    [Jeremy]: /team/#d3arWatson
    [Fabio]: /team/#bikebilly
    [Josh]: /team/#joshlambert
    [Mark]: /team/#MarkPundsack
    [Ashish]: /team/#kuthiala
    [William]: /team/#thewilliamchia
    [James]: /team/#jamesramsay
    [Job]: /team/#Jobvo
    [John]: /team/#j_jeremiah
    [Victor]: /team/#victorwuky
    [Daniel]: /team/#danielgruesso
    [Tommy]: /team/#tommy.morgan
    [Dalia]: /team/#dhavens
    [Andreas]: /team/#andreasmarc
    [Pages]: /features/pages/
    [Geo]: /features/gitlab-geo/
    [Continuous Integration (CI)]: /features/gitlab-ci-cd/
    [Continuous Delivery (CD)]: /features/gitlab-ci-cd/
    [Subgroups]: /features/subgroups/
    [Service Desk]: /features/service-desk/
