---
layout: markdown_page
title: Directly Responsible Individuals
---

## On this page
{:.no_toc}

- TOC
{:toc}


#### What is a directly responsible individual?

[Apple coined the term](http://fortune.com/2011/08/25/how-apple-works-inside-the-worlds-biggest-startup/) "directly responsible individual" (DRI) to refer to the one person with whom the buck stopped on any given project. The idea is that every project is assigned a DRI who is ultimately held accountable for the success (or failure) of that project.

They likely won't be the only person working on their assigned project, but it's ["up to that person to get it done or find the resources needed."](https://originalfuzz.com/blogs/magazine/83782148-the-directly-responsible-individual)

The DRI might be a manager or team leader, they might even be an executive. Or, they may themselves be individually responsible for fulfilling all the needs of their project. The selection of a DRI and their specific role will vary based on their own skillset and the requirements of their assigned task. What's most important is that they're empowered.

<blockquote align="center"><i>The one person with whom the buck stopped</i></blockquote>


#### DRIs and our Values
<img align="right" src="/images/training/jobDRI.png" alt="JobDRI" width="300px"/>

**At the end of the day, it's about [results](/handbook/values/#results) and [efficiency](/handbook/values/#efficiency).** DRIs work conceptually because they leave no room for ambiguity about who has the final say on all questions that arise within a project or team.

**Assigning one, ultimately responsible person to a project might seem to impair our ability to [collaborate](/handbook/values/#collaboration) effectively at first glance, but that's misleading.** The DRI should be wholly invested in their assignment and welcome collaboration in order to succeed. While they're empowered to make all final decisions, they should know how and when to trust in the experience and judgment of their teams and peers.

**Of course, when things do go wrong, it's also the DRI who (usually) takes the fall** as was the case when Scott Forestall, then iOS senior vice president, was forced to resign after he ["refused to sign the letter apologizing"](http://fortune.com/2012/10/29/inside-apples-major-shakeup/) for Apple's infamously error-laden Maps app redesign in 2011.

#### Characteristics of a DRI

<img src="/images/training/success_image.png" alt="success_image" width="300px"/>

As a manager, when you are thinking of assigning or even promoting someone to a DRI, there are some suggested characteristics that you should keep in mind. [Mike Brown](http://brainzooming.com/about-brainzooming/mike-brown/) in his November 2015 article, [project management 8 characteristics of a dri](http://brainzooming.com/project-management-8-chracteristics-of-a-dri/25340/) lists the following:

1. Detail-orientated without ever losing a strong strategic perspective
2. Calm under the pressure of implementation and deadlines
3. A strong listener with great skill at asking questions
4. Able to vary the direction of project (or tactic or task) in smart ways to keep moving toward the objective
5. Adept at anticipating potential problems and addressing them early
6. Able to successfully interact at senior and junior levels within the organization
7. Resilient in order to recover from setbacks
8. Consistent in how they respond to comparable situations

The DRI is also part of a team, a team needs to be motivated and aligned on achieving the steps to get to success. The DRI will also be responsible for making sure the team gets there.

#### Communication and Feedback

Following on from the last two sentences, a DRI should be able to articulate the objectives, check progress and give and receive feedback. This will ensure the DRI can change direction or plan ahead to avoid any setbacks.

<img src="https://imgs.xkcd.com/comics/misinterpretation.png" alt="misinterpretation"/>
Source: https://www.xkcd.com/

At GitLab we communicate and work asynchronously, you can read more about it on [this page](https://about.gitlab.com/handbook/communication/)


##### Feedback

One thing to consider when a DRI needs to give or receive feedback is that they may not be the actual manager of the other members of the team. Giving or receiving feedback is tough and we have looked at this in our previous [Guidance on Feedback Training](handbook/people-operations/guidance-on-feedback).

How do you go about it when you are a peer in addition to being a DRI?




#### Further Reading

1. [How well does Apple's DRI model work in practice](https://www.forbes.com/sites/quora/2012/10/02/how-well-does-apples-directly-responsible-individual-dri-model-work-in-practice/#4d83402d194c)
2. [Matthew Mamet, DRI](https://medium.com/@mmamet/directly-responsible-individuals-f5009f465da4)
3. [GitLab Handbook, Guidance on Feedback](https://about.gitlab.com/handbook/people-operations/guidance-on-feedback/)
