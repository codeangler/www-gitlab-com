---
layout: markdown_page
title: "Ultimate Value"
---


## GitLab Ultimate
There are four tiers of GitLab; Core, Starter, Premium and Ultimate. Only Ultimate includes the features, capabability and direction to support enterprise IT transformation to high velocity delivery without sacrificing security, compliance or enteprise governance. Ultimate is intended to be business focused and enable businesses to acheive a digital transformation by optimizing and accelerating delivery while managing priorities, security, risk, and compliance.

*note:* If you want to learn about the other GitLab tiers see the [pricing page](about.gitlab.com/pricing) for details.



### **Porfolio Management**  
Planning and managing work across multiple projects, products, and groups is a longstanding challenge where enterprise leaders try to optimize their tallent, budget and time to deliver the most value to their business. In GitLab Ultimate, we are delivering specific capabilities to support portfolio planning and execution.

| Portfolio Mgt     | Value |
| --------- | ------------ |
| Epics |  Organize, plan and manage work across the group, collecting multiple issues together.  |
| Roadmaps | Visualize multiple epics across time in order to plan when future features will ship.   |
| [*VSM Workflow Analytics*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/7269) | *Visualize the end to end value stream to identify and resolve bottlenecks.* |
| [*Risk Management* (future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/3978) | *Manage risk of epics not being completed on time.* |
| [*What-If Scenario Planning*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/3979) | *Visualize potential impact in the overall portfolio if you were to make a change.* |


### **Security** 
Delivering code at DevOps velocity MUST include security as a core element in the pipeline. GitLab ultimate weaves security into the pipeline to provide early and actionable feedback to the development team.  

| Security    | Value |
| --------- | ------------ |
| Static Application Security Testing | Evaluates the current code, checking for potential security issues at the code level.   |
| Dynamic Application Security Testing | Analyzes the review application to identify potential security issues.  |
| Dependency Scanning |  Evaluates the project dependencies to identify potential security issues   |
| Container Scanning |  Analyzes Docker images and checks for potential security issues  |
| Security Dashboard | Visualize the latest security status of the default branch for each project. |
| [*Security Metrics and Trends* (future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/6954)| *Metrics and historical data about how many vulns have been spotted, addressed, solved, and how much time was spent for the complete cycle.* |



### **Compliance**

| Compliance    | Value |
| --------- | ------------ |
| License Mgt |  Identify the presence of new software licenses included in your project. Approve or deny the inclusion of a specific license. |
| [*CD with SOC 2 Compliance*(future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/4120) | *Support SOC 2 compliance* |

### **Operations**

| Operations    | Value |
| --------- | ------------ |
| Kubernetes Cluster Health Monitoring | Track cluster CPU and Memory utilization. Keeping an eye on cluster resources can be important, if the cluster runs out of memory pods may be shutdown or fail to start.   |
| Kubernetes Cluster Logs | View the logs of running pods in connected Kubernetes clusters so that developers can avoid having to manage console tools or jump to a different interface. |
| App Perf. Alerts | Rspond to changes to your application performance with alerts for custom metrics. |
| [*Operations Dasboard* (future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/1788) | *a holistic view of the overall health of your company's operations.*  |
| [*Anomoly Alerts* (future)](https://gitlab.com/gitlab-org/gitlab-ee/issues/3610) | *alerting based on deviation from the weekly mean.* |

### **Other**

| Other    | Value |
| --------- | ------------ |
| Chat Ops | Execute common actions directly from chat, with the output sent back to the channel. |
| Guest Users|  Guest users don’t count towards the license count.  |
