title: Axway
cover_image: '/images/blogimages/axway-case-study-image.png'
cover_title: |
  Axway realizes a 26x faster DevOps cycle by switching from Subversion to GitLab
cover_description: |
  To help customers better leverage their data, Axway stays ahead of the market by releasing new code faster, using GitLab for source code management
twitter_image: '/images/blogimages/axway-case-study-image.png'

customer_logo: 'images/case_study_logos/axway-logo.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Computer software
customer_location: Paris, France and Phoenix, Arizona
customer_employees: 1,001-5,000
customer_overview: |
  Axway is a publicly held information technology company specializing in enterprise software, including business analytics, API management, and mobile app development
customer_challenge: |
  Connecting globally distributed R&D teams to implement DevOps
customer_solution: GitLab Premium

key_benefits:
  - |
    On-premise solution for enterprise source code management coupled with the most advanced feature set on the market
  - |
    World-class integrations and flexible APIs enabled seamless workflows between GitLab, JIRA, and Jenkins
  - |
    Simplified administration using one centralized instance for all repositories

customer_stats:
  - stat: 600+
    label: developers onboarded
  - stat: "3,000"
    label: projects migrated
  - stat: 26x
    label: faster cycle time

customer_study_content:
  - title: the customer
    subtitle: Change agents tackling the toughest data challenges
    content:
      - |
        Inventors of the world’s first business rules engine, Règle du Jeu (RDJ),
        Axway has been helping businesses unlock the value of their data for
        nearly two decades. Their cloud-enabled data integration and engagement
        platform, Axway AMPLIFY, orchestrates cohesive customer experience
        networks, enabling organizations to generate the speed, power, and
        agility required to meet skyrocketing customer expectations.
      - |
        As change agents for data integration, keeping pace and adapting quickly
        to today’s fast and fluid digital customer is mission critical for
        Axway’s research and development (R&D) engineering services teams.


  - title: the challenge
    subtitle: Legacy source code management and toolchain complexity limits worldwide collaboration
    content:
      - |
        Serving over 11,000 companies spanning 100 countries, Axway’s R&D
        engineering services teams are spread across continents and time zones.
        With the teams working from nine different sites and using Subversion
        (SVN) on local servers for source control management (SCM), collaboration
        between the globally distributed teams was suffering, preventing them
        from implementing DevOps.
      - |
        “Our legacy toolchain was complex, disconnected, and difficult to manage
        and maintain,” said Eric Labourdette, Head of Global R&D Engineering
        Services and responsible for worldwide R&D operations including DevOps.
        “SVN was difficult to administer and didn’t scale, causing extra
        administrative overhead. It became unmanageable for our team size.”
      - |
        As the organization looked to move to a microservices architecture and
        implement DevOps, they needed a solution for  SCM that could foster
        collaboration across locations and projects, and support DevOps
        practices like continuous integration and automated deploys.


  - title: the solution
    subtitle: GitLab for SCM enables DevOps
    content:
      - |
        A modern solution that can be hosted on-premise, GitLab provided a
        single instance of source code for the entire company. Leveraging a
        Git-based workflow, Axway’s worldwide R&D teams can now easily share and
        collaborate on code with nearly zero downtime and minimal administrative
        overhead. Leveraging the most robust Lightweight Directory Access
        Protocol (LDAP) support of any SCM provider, with GitLab, Axway is able to manage
        access and permissions for 600 enterprise developers securely and
        consistently.

  - blockquote: GitLab offered the most advanced feature set on the market.
    attribution: Eric Labourdette
    attribution_title: Head of Global R&D Engineering Services

  - content:
      - |
        “GitLab met our requirements and gave us the best value for the price,”
        said Labourdette. “The feature set with GitLab on-premise was more
        advanced than GitHub and we saw the pace and development [of GitLab]
        moving faster with a community that was active in delivering and
        contributing.”
      - |
        Collaboration between development and operations teams improved, too.
        Using GitLab’s Jenkins integration, developers can trigger their own
        builds and see the output of the pipeline status in the same view as
        their code within GitLab.


  - title:  the results
    subtitle: Faster cycle time and soaring adoption
    content:
      - |
        Now, with developers empowered to self-serve with minimal guidelines,
        the DevOps teams at Axway have improved their cycle time from annual
        releases down to two weeks for some products.
      - |
        In using a bottom-up approach to choose their SCM solution, adoption of
        GitLab was unsurprisingly fast and natural. In just one year, 630 users
        were onboarded and 3,000 projects were migrated. “Everyone was asking
        for it,” said Labourdette. “Now, every single developer is running on
        GitLab without enforcement.”
